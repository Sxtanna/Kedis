package com.sxtanna

import com.sxtanna.KUtils.Files
import com.sxtanna.base.KedisTask
import com.sxtanna.config.KedisConfig
import com.sxtanna.pubsub.KedisPubSub
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisPool
import redis.clients.jedis.JedisPoolConfig
import java.io.File

class Kedis(val config: KedisConfig, var defaultDB: Int = 0) : (KedisTask.() -> Unit) -> Unit {

	private lateinit var pool: JedisPool
	private val pubSubs = mutableSetOf<KedisPubSub>()


	fun start() {
		val jedisConfig = JedisPoolConfig().apply {
			maxTotal = config.pool.maxSize
			maxIdle = config.pool.idleSize
			testOnBorrow = true
		}

		pool = JedisPool(jedisConfig, config.address, config.port, config.pool.timeout, config.auth)
	}

	fun stop() {
		pubSubs.forEach(KedisPubSub::disable).also { pubSubs.clear() }
		pool.destroy()
	}


	fun resource(): Jedis = checkNotNull(pool.resource) { "Could not get resource from pool" }

	operator fun invoke(db: Int = defaultDB, block: KedisTask.() -> Unit) {
		this {
			select(db)
			this.block()
		}
	}

	override fun invoke(block : KedisTask.() -> Unit) {
		resource().use {
			KedisTask(it).block()
		}
	}

	fun register(pubSub: KedisPubSub, vararg channel: String) {
		this {
			pubSubs.add(pubSub)
			jedis.subscribe(pubSub, *channel)
		}
	}


	companion object {

		fun loadConfig(file: File): KedisConfig {
			return checkNotNull(Files.loadFromFile(file, KedisConfig.DEFAULT)) { "Loaded config from file $file is null" }
		}

		fun saveConfig(config : KedisConfig, file : File) {
			Files.saveToFile(file, config)
		}

		fun loadOrCreate(file : File): KedisConfig {
			val loaded = loadConfig(file)
			if (file.exists().not()) saveConfig(loaded, file)

			return loaded
		}
	}

}