package com.sxtanna.config

import com.sxtanna.config.KedisConfig.PoolConfig

data class KedisConfig
(val address: String,
 val auth: String,
 val port: Int = 6379,
 val pool: PoolConfig = PoolConfig()) {


	data class PoolConfig(val maxSize: Int = 200, val idleSize: Int = 200, val timeout: Int = 0)


	companion object {

		val DEFAULT = KedisConfig("", "")

	}

}