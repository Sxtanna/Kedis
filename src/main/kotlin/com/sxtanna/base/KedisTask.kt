package com.sxtanna.base

import redis.clients.jedis.Jedis

class KedisTask(val jedis: Jedis) {

	operator fun invoke(block: KedisTask.() -> Unit) = this.block()

	operator fun (() -> Unit).contains(db: Int): Boolean {
		val previous = jedis.db.toInt()
		select(db)
		this()
		select(previous)
		return true
	}

	fun select(db: Int): Unit {
		jedis.select(db)
	}


	//region Basic Key
	operator fun set(key: String, value: Any) {
		jedis.set(key, value.toString())
	}

	operator fun get(key: String): String? {
		return jedis.get(key)
	}

	operator fun contains(key: String): Boolean {
		return jedis.exists(key)
	}

	fun rem(vararg key: String): Boolean {
		return jedis.del(*key).toInt() == key.size
	}
	//endregion


	//region Hash
	fun setHash(key: String, hashKey: String, value: Any) {
		jedis.hset(key, hashKey, value.toString())
	}

	fun getHash(key: String, hashKey: String): String? {
		return jedis.hget(key, hashKey)
	}

	fun getHash(key : String): Map<String, String> {
		return jedis.hgetAll(key)
	}

	fun getHashSize(key : String): Int {
		return jedis.hlen(key).toInt()
	}

	fun hasHash(key: String, hashKey: String): Boolean {
		return jedis.hexists(key, hashKey)
	}

	fun remHash(key: String, vararg hashKey: String): Boolean {
		if (hashKey.isEmpty()) return rem(key)
		return jedis.hdel(key, *hashKey).toInt() == hashKey.size
	}
	//endregion


	//region Sorted Set
	fun setZRank(key: String, rank: Long, value: Any) {
		jedis.zadd(key, rank.toDouble(), value.toString())
	}

	fun getZRank(key: String, value: Any): Long? {
		return jedis.zrank(key, value.toString())
	}

	fun getZRankSize(key : String): Int {
		return jedis.zcard(key).toInt()
	}

	fun hasZRank(key: String, value: Any): Boolean {
		return getZRank(key, value) != null
	}

	fun remZRank(key: String, vararg value: String): Boolean {
		if (value.isEmpty()) return rem(key)

		return jedis.zrem(key, *value).toInt() == value.size
	}
	//endregion


	//region UnSorted Set
	fun setSet(key : String, vararg value : Any) {
		jedis.sadd(key, *value.toStrings())
	}

	fun getSet(key : String): Set<String> {
		return jedis.smembers(key)
	}

	fun getSetSize(key : String): Int {
		return jedis.scard(key).toInt()
	}

	fun hasSet(key : String, value : Any): Boolean {
		return jedis.sismember(key, value.toString())
	}

	fun remSet(key : String, vararg value : Any) {
		jedis.srem(key, *value.toStrings())
	}
	//endregion




	fun push(channel: String, message: String) {
		jedis.publish(channel, message)
	}


	private fun Array<out Any>.toStrings() = this.map(Any::toString).toTypedArray()

}