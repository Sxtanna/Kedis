package com.sxtanna.ext

import com.sxtanna.Kedis
import com.sxtanna.pubsub.KedisPubSub

const val REDIS_DEFAULT_PORT = 6379


fun String.listen(kedis: Kedis, block: (String) -> Unit): KedisPubSub {
	return object : KedisPubSub(kedis) {
		override fun pull(channel: String, message: String) = block(message)

	}.apply { enable() }
}
