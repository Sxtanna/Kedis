package com.sxtanna.pubsub

import com.sxtanna.Kedis
import redis.clients.jedis.JedisPubSub

abstract class KedisPubSub(val kedis: Kedis, vararg val channels: String) : JedisPubSub() {

	var enabled = false
		private set


	@Deprecated("Do not use, use invoke instead", level = DeprecationLevel.ERROR)
	override fun onMessage(channel: String?, message: String?) {
		val nonNullChannel = requireNotNull(channel) { "Channel cannot be null" }
		val nonNullMessage = requireNotNull(message) { "Message cannot be null" }

		pull(nonNullChannel, nonNullMessage)
	}

	fun enable() {
		if (enabled) throw IllegalStateException("This PubSub is already enabled")

		kedis.register(this, *channels)

		enabled = true
		onEnable()
	}

	fun disable() {
		if (enabled.not()) throw IllegalStateException("This PubSub is already disabled")

		unsubscribe()

		enabled = false
		onDisable()
	}

	abstract fun pull(channel: String, message: String)

	fun push(channel: String, message: String) {
		check(enabled) { "Cannot push data to channel $channel while pubSub isn't enabled" }

		kedis.invoke {
			push(channel, message)
		}
	}

	open fun onEnable(): Unit = Unit

	open fun onDisable(): Unit = Unit

}